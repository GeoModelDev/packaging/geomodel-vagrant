#!/usr/bin/env bash

# Install dependecies
sudo apt-get update && sudo apt-get install -y git cmake wget unzip build-essential freeglut3-dev libboost-all-dev qt5-default mercurial libeigen3-dev libxerces-c-dev libsqlite3-dev


# Install X11 dependencies to run gmex inside the VM,
# on your own regular desktop/laptop you probably don't need that.
sudo apt install -y x11-apps xauth
# NOTE:
# In order to run gmex from the VM, you then connect to it through:
# vagrant ssh -- -X
#  on macOS, you should install and run the 'XQuartz' application before trying this.
#       on Linux, X11 is installed by default in any standard distribution


# Create work directory and enter it
mkdir geomodeldev
cd geomodeldev

# Build Coin3D
wget https://bitbucket.org/Coin3D/coin/downloads/coin-4.0.0-src.zip
unzip coin-4.0.0-src.zip -d coin-sources
mv coin-sources/* coin
mkdir build_coin
cd build_coin
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../install ../coin
make # NOTE: you might want to use 'make -j' on your machine, to speed up the build
make install
cd ..

# Build SoQt
hg clone https://rmbianchi@bitbucket.org/rmbianchi/soqt
mkdir build_soqt
cd build_soqt
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../install ../soqt
make -j2 # NOTE: you might want to use 'make -j' on your machine, to speed up the build
make install
cd ..

# Build GeoModelCore
git clone https://gitlab.cern.ch/GeoModelDev/GeoModelCore.git
mkdir build_core
cd build_core
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../install ../GeoModelCore
make -j2 # NOTE: you might want to use 'make -j' on your machine, to speed up the build
make install
cd ..

# Build GeoModelCore
git clone https://gitlab.cern.ch/GeoModelDev/GeoModelCore.git
mkdir build_core
cd build_core
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../install ../GeoModelCore
make -j2 # NOTE: you might want to use 'make -j' on your machine, to speed up the build
make install
cd ..

# Build GeoModelIO
git clone https://gitlab.cern.ch/GeoModelDev/GeoModelIO.git
mkdir build_io
cd build_io
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../install ../GeoModelIO
make -j2 # NOTE: you might want to use 'make -j' on your machine, to speed up the build
make install
cd ..

# Build the GeoModelTools
git clone https://gitlab.cern.ch/GeoModelDev/GeoModelTools.git
mkdir build_tools
cd build_tools
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../install ../GeoModelTools
make -j2 # NOTE: you might want to use 'make -j' on your machine, to speed up the build
make install
cd ..

# Build the GeoModelATLAS/GeoModelDataManagers
git clone https://gitlab.cern.ch/GeoModelATLAS/GeoModelDataManagers.git
mkdir build_managers
cd build_managers
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../install ../GeoModelDataManagers
make -j2 # NOTE: you might want to use 'make -j' on your machine, to speed up the build
make install
cd ..

# Build atlas/GeoModelPlugins
git clone https://gitlab.cern.ch/atlas/GeoModelPlugins.git
mkdir build_plugins
cd build_plugins
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../install ../GeoModelPlugins
make -j2 # NOTE: you might want to use 'make -j' on your machine, to speed up the build
make install
cd ..

# Build GeoModelVisualization
git clone https://gitlab.cern.ch/GeoModelDev/GeoModelVisualization.git
mkdir build_viz
cd build_viz
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../install -DGEOMODEL_USE_BUILTIN_JSON=TRUE ../GeoModelVisualization
make -j2 # NOTE: you might want to use 'make -j' on your machine, to speed up the build
make install
cd ..

# Setup environment variables
export LD_LIBRARY_PATH=${PWD}/install/lib/


# Download an example, full-ATLAS geometry file
wget https://gitlab.cern.ch/GeoModelATLAS/geometry-data/-/raw/master/geometry/geometry-ATLAS-R2-2016-01-00-01_wSPECIALSHAPE.db?inline=false -O geometry-ATLAS-R2-2016-01-00-01_wSPECIALSHAPE.db



# if ! [ -L /var/www ]; then
#   rm -rf /var/www
#   ln -fs /vagrant /var/www
# fi
